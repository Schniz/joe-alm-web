import React from 'react';
import Relay from 'react-relay';

export default class Assignee extends React.Component {
  static propTypes = {
    data: React.PropTypes.shape({
      name: React.PropTypes.string.isRequired,
      username: React.PropTypes.string.isRequired,
    }),
  };

  render() {
    const { data } = this.props;

    if (!data) return <noscript />;

    return (
      <span title={ data.name }>
        <a href={ `${process.env.GITLAB_HOST}/${data.username}` }>
          @{ data.username } hagever!!!
        </a>
      </span>
    );
  }
}
