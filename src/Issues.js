import React from 'react';
import Relay from 'react-relay';
import Issue from './containers/Issue';
import Layout from './Layout';

export default class Issues extends React.Component {
  static propTypes = {
    issues: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    title: React.PropTypes.string,
  };

  render() {
    const { issues, title } = this.props;
    return (
      <Layout padding='0.1em'>
        <h2>{ title }</h2>
        { issues.map((issue, i) => <Issue isOdd={ i % 2 === 0 } key={issue.iid} data={issue} />) }
      </Layout>
    );
  }
}
