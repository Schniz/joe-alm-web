import React from 'react';
import Issues from './Issues';
import Milestone from './containers/Milestone';
import MilestoneSelectionItem from './MilestoneSelectionItem';
import Layout from './Layout';

export default class Project extends React.Component {
  static propTypes = {
    data: React.PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.state = {
      milestone: null,
    };
  }

  render() {
    const { data } = this.props;

    return (
      <Layout fullHeight flexbox column>
        <h1>
          Project: {data.name}
        </h1>
        <Layout flexbox flex={1}>
          <Layout>
            <h2>Milestones</h2>
            <ul>
              { data.milestones.map(e => <MilestoneSelectionItem key={e.id} data={ e } onSelected={ id => this.setState({ milestone: id }) } />) }
            </ul>
          </Layout>
          <Issues title='Backlog' issues={ data.backlog } />
          {
            this.state.milestone ? (
              <Milestone data={ data.milestones.find(milestone => this.state.milestone === milestone.id) } />
              ) : (
              <Layout flex={ 4 } deadCenter>
                Please select a milestone
              </Layout>
              )
          }
        </Layout>
      </Layout>
    );
  }
}
