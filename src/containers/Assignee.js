import React from 'react';
import Relay from 'react-relay';

import Assignee from '../Assignee';

export default Relay.createContainer(Assignee, {
  fragments: {
    data: () => Relay.QL`
      fragment on Owner {
        username,
        name
      }
    `
  }
});
