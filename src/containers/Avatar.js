import Relay from 'react-relay';
import Avatar from '../Avatar';

export default Relay.createContainer(Avatar, {
  fragments: {
    data: () => Relay.QL`
      fragment on Owner {
        avatar,
        name,
        username,
      }
    `
  }
});
