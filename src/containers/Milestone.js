import React from 'react';
import Relay from 'react-relay';
import Issue from './Issue';

import Milestone from '../Milestone';

export default Relay.createContainer(Milestone, {
  fragments: {
    data: () => Relay.QL`
      fragment on Milestone {
        title,
        todo { iid, ${ Issue.getFragment('data') } },
        wip { iid, ${ Issue.getFragment('data') } },
        review { iid, ${ Issue.getFragment('data') } },
        done { iid, ${ Issue.getFragment('data') } }
      }
    `
  }
});
