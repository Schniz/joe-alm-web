import Relay from 'react-relay';
import MilestoneSelectionItem from '../MilestoneSelectionItem';

export default Relay.createContainer(MilestoneSelectionItem, {
  fragments: {
    data: () => Relay.QL`
      fragment on Milestone {
        id,
        title
      }
    `
  }
});
