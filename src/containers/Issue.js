import Relay from 'react-relay';
import React from 'react';
import Assignee from './Assignee';
import Avatar from './Avatar';
import Issue from '../Issue';

export default Relay.createContainer(Issue, {
  fragments: {
    data: () => Relay.QL`
      fragment on Issue {
        title,
        iid,
        createdAt,
        assignee {
          ${Assignee.getFragment('data')}
          ${Avatar.getFragment('data')}
        }
        author {
          username
        }
      }
    `
  }
});

