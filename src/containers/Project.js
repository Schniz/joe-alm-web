import Relay from 'react-relay';
import Issue from './Issue';
import Milestone from './Milestone';
import MilestoneSelectionItem from './MilestoneSelectionItem';

import Project from '../Project';

export default Relay.createContainer(Project, {
  fragments: {
    data: () => Relay.QL`
      fragment on Project {
          name
          backlog {
            iid
            ${Issue.getFragment('data')}
          }
            milestones {
              id,
              title,
              ${Milestone.getFragment('data')},
              ${MilestoneSelectionItem.getFragment('data')},
            }
      }
    `
  }
});

