import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Relay from 'react-relay';
import Project from './containers/Project';

Relay.injectNetworkLayer(
  new Relay.DefaultNetworkLayer('http://localhost:8081/graphql', {
    headers: {
      token: process.env.TOKEN,
    }
  })
);

class ProjectRoute extends Relay.Route {
  static routeName = 'ProjectRoute';
  static queries = {
    data: ((Component) => {
      // Component is our Item
      return Relay.QL`
      query { project(path: "Schniz/joe-alm-test-project") { ${Component.getFragment('data')} } }
      `}),
  };
}

const root = <Relay.RootContainer
  Component={Project}
  route={new ProjectRoute()}
/>;

ReactDOM.render(root, document.getElementById('root'));
