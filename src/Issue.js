import React from 'react';
import Relay from 'react-relay';
import Assignee from './containers/Assignee';
import Avatar from './containers/Avatar';
import Layout from './Layout';
import TimeAgo from 'react-timeago';

export default class Issue extends React.Component {
  static propTypes = {
    data: React.PropTypes.shape({
      iid: React.PropTypes.number,
      title: React.PropTypes.string,
      assignee: React.PropTypes.object,
    }).isRequired,
    isOdd: React.PropTypes.bool,
  };

  constructor(props) {
    super(props);
    this.state = {
      hovered: false,
    };
  }

  render() {
    const { data, isOdd } = this.props;
    return (
      <li
        onMouseEnter={ () => this.setState({ hovered: true }) }
        onMouseLeave={ () => this.setState({ hovered: false }) }
        style={{
          listStyle: 'none',
          padding: '0.4em 0.2em',
          backgroundColor: isOdd ? 'transparent' : 'rgba(0,0,0,.05)'
        }}
      >
        <Layout
          flexbox
          noflex
          href={ `./issue/${data.iid}` }
        >
          <Avatar size='2em' data={ data.assignee } round />
          <Layout marginLeft={ data.assignee ? '0.3em' : '' }>
            { data.title }
            <Layout textColor='grey' textSize='0.8em' alignRight>
              #{ data.iid } by { data.author.username }, <TimeAgo date={ data.createdAt } />
            </Layout>
          </Layout>
        </Layout>
      </li>
    )
  }
}
