import React from 'react';

export default class MilestoneSelectionItem extends React.Component {
  static propTypes = {
    data: React.PropTypes.shape({ id: React.PropTypes.string }).isRequired,
    onSelected: React.PropTypes.func.isRequired,
  };

  render() {
    console.log(this.props);
    const { data, onSelected } = this.props;

    return (
      <li
        key={ data.id }
        onClick={ onSelected.bind(null, data.id) }
      >
        { data.title }
      </li>
    );
  }
}
