import React from 'react';

export default class Avatar extends React.Component {
  static propTypes = {
    data: React.PropTypes.shape({
      avatar: React.PropTypes.string.isRequired,
      username: React.PropTypes.string.isRequired,
      name: React.PropTypes.string.isRequired,
    }),
    size: React.PropTypes.string,
    round: React.PropTypes.bool,
    nolink: React.PropTypes.bool,
  };

  render() {
    const { data, size, round, nolink } = this.props;
    if (!data) return null;
    const title = `${data.username} (${data.name})`;
    const img = (
      <img alt={ data.name } style={{
        height: size,
        width: size,
        borderRadius: round ? '50%' : 0,
        border: 0,
      }} src={ data.avatar } />
    );

    if (nolink) return img;

    return (
      <a href={ `${process.env.GITLAB_HOST}/u/${data.username}` } title={ title }>
        { img }
      </a>
    );
  }
}
