import React from 'react';
import Relay from 'react-relay';
import Issues from './Issues';
import Layout from './Layout';
import DocumentTitle from 'react-document-title';

export default class Milestone extends React.Component {
  static propTypes = {
    data: React.PropTypes.shape({
      todo: React.PropTypes.array,
      wip: React.PropTypes.array,
      review: React.PropTypes.array,
      done: React.PropTypes.array,
    }).isRequired,
  };

  render() {
    const { data } = this.props;
    return (
      <DocumentTitle title={ `Reviewing ${data.title}` }>
        <Layout flexbox flex={ 4 }>
          <Issues title='Todo' issues={ data.todo } />
          <Issues title='WIP' issues={ data.wip } />
          <Issues title='Review' issues={ data.review } />
          <Issues title='Done' issues={ data.done } />
        </Layout>
      </DocumentTitle>
    )
  }
}
