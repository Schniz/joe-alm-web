import React from 'react';

function deadCenterStyle(isDeadCenter) {
  return !isDeadCenter ? {} : {
    alignSelf: 'center',
    textAlign: 'center',
  };
}

export default class Container extends React.Component {
  static propTypes = {
    flex: React.PropTypes.number,
    deadCenter: React.PropTypes.bool,
    flexbox: React.PropTypes.bool,
    noflex: React.PropTypes.bool,
    marginLeft: React.PropTypes.string,
    alignRight: React.PropTypes.bool,
    textColor: React.PropTypes.string,
    textSize: React.PropTypes.string,
    padding: React.PropTypes.string,
    column: React.PropTypes.string,
    fullHeight: React.PropTypes.bool,
  };

  static defaultProps = {
    flex: 1,
  };

  render() {
    const { fullHeight, column, textColor, padding, textSize, flex, deadCenter, flexbox, noflex, marginLeft, alignRight } = this.props;
    return (
      <div
        style={{
          flex: noflex ? '0 0 auto' : flex,
          textAlign: alignRight ? 'right' : 'left',
          ...(flexbox ? { display: 'flex' } : {}),
          marginLeft: marginLeft,
          color: textColor,
          fontSize: textSize,
          padding: padding,
          fontFamily: 'Helvetica, Arial',
          ...deadCenterStyle(deadCenter),
          flexDirection: column ? 'column' : 'row',
          minHeight: fullHeight ? '100vh' : undefined,
        }}
        {...this.props}
      />
    );
  }
}
