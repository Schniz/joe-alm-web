# joe-alm-web
A web client for [joe-alm](https://gitlab.com/Schniz/joe-alm)

# Tech
- React.js (with hot reloading)
- Relay.js
- React Router (soon)
